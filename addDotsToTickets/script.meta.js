// ==UserScript==
// @name         Add dots to tickets
// @namespace    http://tampermonkey.net/
// @version      0.2.5
// @description  Decorate Xymon tickets with present colour
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/addDotsToTickets/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/addDotsToTickets/script.meta.js
// @author       Frank Lee
// @match        https://tickets.ch.cam.ac.uk/rt/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_addStyle
// ==/UserScript==
