Adds a coloured dot to the subject of Xymon tickets in RT reflecting the latest status of that test.

(Dots with a blue circle around are disabled; the central colour reports the colour of the most recent report.)
