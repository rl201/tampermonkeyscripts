// ==UserScript==
// @name         Add dots to tickets
// @namespace    http://tampermonkey.net/
// @version      0.2.5
// @description  Decorate Xymon tickets with present colour
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/addDotsToTickets/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/addDotsToTickets/script.meta.js
// @author       Frank Lee
// @match        https://tickets.ch.cam.ac.uk/rt/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_addStyle
// ==/UserScript==
/* globals jQuery */

function getxymontickets() {
    return jQuery('td.collection-as-table a:contains("Xymon")')
        .filter(function() {
            return /Xymon /.test(jQuery(this).text());
        })
        .map(function() {
            var r = jQuery(this).text().match(/(\S+.cam.ac.uk):(\S+) /);
            if (r !== null) {
                return {
                    test: '^'+r[2]+'$',
                    host: r[1]
                };
            }
        })
        .toArray();
}

function givexymondots(data) {
    if (data.result != 'OK') {
        return;
    }
    jQuery.each(data.data, function(i, e) {
        if (e.result.length==0) {
            // "continue" for "each" loops.
            return 1
        }
        var cssclass = 'xymon-' + e.result[0].color;
        if (e.result[0].color == 'blue') {
            var ncol = ((e.result[0].line1.split(' '))[0]);
            console.log(ncol);
            cssclass = 'xymon-blue-' + ncol;
        }
        jQuery('td.collection-as-table a:contains("Xymon"):contains("' + e.result[0].hostname + ':' + e.result[0].testname + '")').addClass(cssclass).addClass('xymon-dot');
    });
}

function addDotsToTickets() {
    var targetNode = jQuery('div#delayed_ticket_history:contains("Loading...")');
    if (targetNode.length > 0) {
        window.setTimeout(addDotsToTickets, 100); // Give it another 100ms
        return;
    }
    var url = '/api/rest/v1/xymondboard5.php';
    jQuery.ajax(url, {
        data: JSON.stringify(getxymontickets()),
        contentType: 'application/json',
        type: 'POST',
        success: givexymondots
    });
};

(function() {
    'use strict';
    GM_addStyle('.collection-as-table span a.xymon-dot::after { content:" "; width: 1.0em; border: 0.3em solid; border-radius: 1em; display: inline-block; height: 1.0em }');
    GM_addStyle('.collection-as-table span a.xymon-blue-yellow::after { border-color: blue; background-color: yellow; }');
    GM_addStyle('.collection-as-table span a.xymon-blue-red::after { border-color: blue; background-color: red; }');
    GM_addStyle('.collection-as-table span a.xymon-blue-green::after { border-color: blue; background-color: limegreen; }');
    GM_addStyle('.collection-as-table span a.xymon-yellow::after { border-color: yellow; background-color: yellow; }');
    GM_addStyle('.collection-as-table span a.xymon-red::after { border-color: red; background-color: red;}');
    GM_addStyle('.collection-as-table span a.xymon-green::after { border-color: limegreen; background-color: limegreen;}');
    GM_addStyle('.collection-as-table span a.xymon-purple::after { border-color: purple; background-color: purple;}');
    GM_addStyle('.collection-as-table td a.xymon-dot::after { content:" "; width: 1.0em; border: 0.3em solid; border-radius: 1em; display: inline-block; height: 1.0em }');
    GM_addStyle('.collection-as-table td a.xymon-blue-yellow::after { border-color: blue; background-color: yellow; }');
    GM_addStyle('.collection-as-table td a.xymon-blue-red::after { border-color: blue; background-color: red; }');
    GM_addStyle('.collection-as-table td a.xymon-blue-green::after { border-color: blue; background-color: limegreen; }');
    GM_addStyle('.collection-as-table td a.xymon-yellow::after { border-color: yellow; background-color: yellow; }');
    GM_addStyle('.collection-as-table td a.xymon-red::after { border-color: red; background-color: red;}');
    GM_addStyle('.collection-as-table td a.xymon-green::after { border-color: limegreen; background-color: limegreen;}');
    GM_addStyle('.collection-as-table td a.xymon-purple::after { border-color: purple; background-color: purple;}');
    addDotsToTickets();
})();
