// ==UserScript==
// @name         CVE Linker
// @namespace    http.//tampermonkey.net/
// @version      0.1.0
// @description  Replace CVE references with hyperlinks
// @author       Adam Thorn
// @match        https://tickets.ch.cam.ac.uk/rt/Ticket/Display.html?*
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/cveLinker/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/cveLinker/script.meta.js
// @grant GM_addStyle

// ==/UserScript==

(function() {
  'use strict';

    GM_addStyle('a.cvelink { text-decoration:underline; text-decoration-style: dotted; }');

  // Function to perform the regex replace
  function linkCVE() {
    var elements = document.querySelectorAll('.messagebody .message-stanza');

    // Loop through each element
    elements.forEach(function(element) {
      // Get the text content of the element
      var text = element.innerHTML;

      // Define the regex pattern
      var regex = /\bCVE-\d{4}-\d+\b/g;

      // Replace matches with hyperlinks
      var replacedText = text.replace(regex, function(match) {

        var link = '';
        if (text.includes("Ubuntu Security Notice")) {
          link = 'https://ubuntu.com/security/' + match;
        } else if (text.includes("Debian Security Advisory")) {
          link = 'https://security-tracker.debian.org/tracker/' + match;
        } else {
          link = 'https://nvd.nist.gov/vuln/detail/' + match;
        }

        return '<a class="cvelink" href="' + link + '">' + match + '</a>';

      });

      // Update the element's HTML with the replaced text
      element.innerHTML = replacedText;
    });
  }

  // Give the page a short while to load
  setTimeout(linkCVE, 2000);

})();
