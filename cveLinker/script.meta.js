// ==UserScript==
// @name         CVE Linker
// @namespace    http.//tampermonkey.net/
// @version      0.1.0
// @description  Replace CVE references with hyperlinks
// @author       Adam Thorn
// @match        https://tickets.ch.cam.ac.uk/rt/Ticket/Display.html?*
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/cveLinker/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/cveLinker/script.meta.js
// @grant        none
// ==/UserScript==
