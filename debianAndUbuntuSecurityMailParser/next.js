// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      2024-01-24
// @description  try to take over the world!
// @author       You
// @match        https://tickets.ch.cam.ac.uk/rt/Ticket/Display.html?*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=cam.ac.uk
// @grant        none
// ==/UserScript==
/* globals jQuery */



function analyseSecurityReport() {
    var targetNode = jQuery('div#delayed_ticket_history:contains("Loading...")');
    if (targetNode.length > 0) {
        window.setTimeout(analyseSecurityReport, 100); // Give it another 100ms
        return;
    }
    analyseUbuntuReport();
    //    analyseDebianReport();
}

const debmap = {
    buster: '10',
    bullseye: '11',
    bookworm: '12',
    trixie: '13',
    forky: '14'
};
const ubuntumap = {
    "24.04": 'noble',
    "23.10": 'manic',
    "23.04": 'lunar',
    "22.10": 'kinetic',
    "22.04": 'jammy',
    "20.04": 'focal',
    "18.04": 'bionic',
    "16.04": 'xenial',
    "14.04": 'trusty'
};


function parseVersion(string) {
    if (typeof string !== 'string' || !string) {
        throw new Error('version string is empty')
    }

    const version = new Version()

    string = string.trim()
    const colon = string.indexOf(':')
    if (colon >= 0) {
        if (colon === 0) {
            throw new Error('epoch in version is empty')
        }
        const epoch = parseInt(string.split(':')[0])
        if (isNaN(epoch)) {
            throw new Error('epoch in version is not a number')
        }
        if (epoch < 0) {
            throw new Error('epoch in version is negative')
        }
        if (epoch > Number.MAX_SAFE_INTEGER) {
            throw new Error('epoch in version is too big')
        }
        if ((colon + 1) === string.length) {
            throw new Error('nothing after colon in version number')
        }
        version.epoch = epoch
    } else {
        version.epoch = 0
    }

    const vstring = string.substring(colon + 1)
    const hyphen = vstring.lastIndexOf('-')
    if (hyphen >= 0) {
        version.version = vstring.substring(0, hyphen)
        version.revision = vstring.substring(hyphen + 1)
        if (version.revision.length === 0) {
            throw new Error('revision number is empty')
        }
    } else {
        version.version = string
        version.revision = ''
    }

    if (version.version.length === 0) {
        throw new Error('version number is empty')
    }
    if (!isDigit(version.version[0])) {
        throw new Error('version number does not start with digit' + version.version[0])
    }
    if (!version.version.match(/^[0-9a-zA-Z.+~:-]+$/)) {
        throw new Error('invalid character in version number')
    }

    if (!version.revision.match(/^[0-9a-zA-Z.+~]*$/)) {
        throw new Error('invalid character in revision number')
    }
    return version
}

function isDigit(c) {
    return typeof c === 'string' && c >= '0' && c <= '9'
}

function isAlpha(c) {
    return typeof c === 'string' && ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
}

function order(c) {
    if (isAlpha(c)) {
        return c.charCodeAt(0)
    } else if (c === '~') {
        return -1
    } else if (c) {
        return c + 256
    } else {
        return 0
    }
}

function compareVersion(a, b) {
    let pA = 0
    let pB = 0
    while (a[pA] || b[pB]) {
        let firstDiff = 0
        while ((a[pA] && !isDigit(a[pA])) || (b[pB] && !isDigit(b[pB]))) {
            const ac = order(a[pA])
            const bc = order(b[pB])

            if (ac !== bc) {
                return ac - bc
            }
            pA++
            pB++
        }
        while (a[pA] === '0') {
            pA++
        }
        while (b[pB] === '0') {
            pB++
        }
        while (isDigit(a[pA]) && isDigit(b[pB])) {
            if (!firstDiff) {
                firstDiff = a.charCodeAt(pA) - b.charCodeAt(pB)
            }
            pA++
            pB++
        }
        if (isDigit(a[pA])) {
            return 1
        }
        if (isDigit(b[pB])) {
            return -1
        }
        if (firstDiff) {
            return firstDiff
        }
    }
    return 0
}

function requestIsOs(i) {
    //console.log(i);
    return i.request.test == 'osver';
}

function requestIsSoftware(i) {
    //console.log(i.request.test);
    return i.request.test == 'software';
}

// Sort out hosts into versions
function handleVersionData(r) {
    //console.log(r);
    // First: Which machines run which version?
    if (r.result != 'OK') {
        console.log('Unexpected result from back-end server');
        return;
    }
    var osresults = r.data.filter(requestIsOs);
    var osToHost = {}
    for (var osresult of osresults) {
        osToHost[osresult.request._meta.os] = {
            distname: osresult.request._meta.distname,
            fixedpkg: osresult.request._meta.fixedpkg,
            hosts: osresult.result.map((x) => x.hostname)
        };
    }
    //console.log(osToHost);
    // Now: Which machines have which package versions?
    var pkgResults = r.data.filter(requestIsSoftware);
    //console.log(pkgResults);
    var pkgToHost = {}
    for (var pkgresult of pkgResults) {
        for (var hostresult of pkgresult.result) {
            if (!Object.hasOwn(pkgToHost, hostresult.msg.pkg)) {
                pkgToHost[hostresult.msg.pkg] = {}
            }
            if (!Object.hasOwn(pkgToHost[hostresult.msg.pkg], hostresult.msg.version)) {
                pkgToHost[hostresult.msg.pkg][hostresult.msg.version] = []
            }
            pkgToHost[hostresult.msg.pkg][hostresult.msg.version].push(hostresult.hostname);
        }
    }
    for (var os in osToHost) {
        var mydiv = jQuery('#_pkgscan_' + osToHost[os].distname);
        mydiv.find(' span.hostcount').text(osToHost[os].hosts.length + ' hosts');
        if (osToHost[os].hosts.length > 0) {
            mydiv.append(jQuery('<ul>'));
            var mylist = mydiv.find('ul');
            mylist.append(jQuery('<li>').text('Package - unsafe - safe'));
            for (var pkg in osToHost[os].fixedpkg) {
                //                console.log(osToHost[os].fixedpkg[pkg]);
                // mylist.append(jQuery('<li>').text(pkg+' - '+osToHost[os].fixedpkg[pkg]));
                var safevers = parseVersion(osToHost[os].fixedpkg[pkg])
                var unsafehosts = [];
                var safehosts = [];
                for (var vers in pkgToHost[pkg]) {
                    var thisVers = parseVersion(vers);
                    //var hosts=pkgToHost[pkg][vers];
                    var thisosvershosts = pkgToHost[pkg][vers].filter(x => osToHost[os].hosts.includes(x));
                    if (thisosvershosts.length > 0) {
                        if (thisVers.compareTo(safevers) < 0) {
                            unsafehosts = unsafehosts.concat(thisosvershosts);
                        } else {
                            safehosts = safehosts.concat(thisosvershosts);
                        }
                    }
                }
                if (unsafehosts.length + safehosts.length > 0) {
                    mylist.append(
                        jQuery('<li>').append(
                            jQuery('<span>').text(pkg + ' - '),
                            jQuery('<a>').attr('href', 'https://hobbit.ch.cam.ac.uk/xymon-cgi/findhost.sh?host=' + (unsafehosts.map((x) => x.replace('.cam.ac.uk', '')).join('%7c'))).text(unsafehosts.length),
                            jQuery('<span>').text(' - '),
                            jQuery('<a>').attr('href', 'https://hobbit.ch.cam.ac.uk/xymon-cgi/findhost.sh?host=' + (safehosts.map((x) => x.replace('.cam.ac.uk', '')).join('%7c'))).text(safehosts.length)
                        )
                    );
                } else {
                    mylist.append(jQuery('<li>').text(pkg + ': Package not installed'));
                }
                // console.log(
            }
        }
    }
}

// Which releases are there? Look up machines running each release.
function analyseUbuntuReport() {
    jQuery('div.message-stanza:contains("Ubuntu Security Notice USN")').each(function(i, e) {
        var msg = jQuery(e).text();
        console.log('Analysing Ubuntu Security Notice');
        var found = msg.split(/\nUbuntu /);
        found.shift();
        found.shift();
        var f;
        var osqs = [];
        var pgqs = {};
        for (f of found) {
            // Trim any trailing guff
            f = f.replace((new RegExp('\n\n.*', 's')), '');
            //console.log(f);
            var ls = f.split(/\n/);
            var osline = ls.shift();
            var v = osline.match(/(\d\d.\d\d)/);
            var osnum = v[1];
            //console.log(v[1])
            var osq = {
                test: 'osver',
                msg: 'ubuntu\\s' + osnum,
                fields: ['hostname'],
                _meta: {
                    distname: ubuntumap[osnum],
                    os: osnum,
                    fixedpkg: {}
                }
            };
            for (var l of ls) {
                l = l.trim();
                var m = l.split(/\s+/);
                if (m[1]) {
                    //console.log(m);
                    pgqs[m[0]] = {
                        test: 'software',
                        msg: '(?<pkg>' + m[0] + ')\\s+(?<version>\\S+)',
                        fields: ['hostname', 'msg']
                    };
                    osq._meta.fixedpkg[m[0]] = m[1];
                }
            }
            osqs.push(osq)
            jQuery('div#_pkgscan_ubuntu_' + ubuntumap[osnum]).remove();
            jQuery(e).append(
                jQuery('<div>').addClass('_pkgscan').attr('id', '_pkgscan_' + ubuntumap[osnum]).append(
                    jQuery('<span>').addClass('osname').text('Ubuntu ' + ubuntumap[osnum] + ' [' + osnum + ']'),
                    jQuery('<span>').addClass('hyphen').text(' - '),
                    jQuery('<span>').addClass('hostcount').text('?')
                )
            );

        }
        //console.log(osqs);
        //console.log(pgqs);

        //console.log(Object.values(pgqs));
        var url = '/api/rest/v1/xymondboard3.php';
        var postdata = osqs.concat(Object.values(pgqs));
        console.log(postdata);
        jQuery.ajax(url, {
            data: JSON.stringify(postdata),
            contentType: 'application/json',
            type: 'POST',
            success: handleVersionData,
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("Status: " + textStatus);
                alert("Error: " + errorThrown);
            }
        });
    });
}

class Version {
    compareTo(other) {
        if (this.epoch > other.epoch) {
            return 1
        }
        if (this.epoch < other.epoch) {
            return -1
        }
        const versionCompare = compareVersion(this.version, other.version)
        if (versionCompare) {
            return versionCompare
        }
        return compareVersion(this.revision, other.revision)
    }
}

(function() {
    'use strict';
    analyseSecurityReport();
})();
