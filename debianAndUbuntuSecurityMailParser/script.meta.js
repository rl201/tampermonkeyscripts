// ==UserScript==
// @name         Ubuntu / Debian security report - count affected machines and report
// @namespace    http://tampermonkey.net/
// @version      0.7.2
// @description  Count machines affected by Ubuntu / Debian security reports
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/debianAndUbuntuSecurityMailParser/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/debianAndUbuntuSecurityMailParser/script.meta.js
// @author       Frank Lee <rl201@cam.ac.uk>
// @match        https://tickets.ch.cam.ac.uk/rt/Ticket/Display.html?*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_addStyle
// @grant        GM_xmlhttpRequest
// @connect      ubuntu.com
// ==/UserScript==
