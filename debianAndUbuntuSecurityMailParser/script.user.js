// ==UserScript==
// @name         Ubuntu / Debian security report - count affected machines and report
// @namespace    http://tampermonkey.net/
// @version      0.7.2
// @description  Count machines affected by Ubuntu / Debian security reports
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/debianAndUbuntuSecurityMailParser/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/debianAndUbuntuSecurityMailParser/script.meta.js
// @author       Frank Lee <rl201@cam.ac.uk>
// @match        https://tickets.ch.cam.ac.uk/rt/Ticket/Display.html?*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_addStyle
// @grant        GM_xmlhttpRequest
// @connect      ubuntu.com
// ==/UserScript==
/* globals jQuery */

function analyseSecurityReport() {
    var targetNode = jQuery('div#delayed_ticket_history:contains("Loading...")');
    if (targetNode.length > 0) {
        console.log('Delayed ticket history still loading');
        window.setTimeout(analyseSecurityReport, 100); // Give it another 100ms
        return;
    }
    console.log('Analysing Ubuntu security report');
    analyseUbuntuReport();
    console.log('Analysing Debian security report');
    analyseDebianReport();
}

const debmap = {
    buster: '10',
    bullseye: '11',
    bookworm: '12',
    trixie: '13',
    forky: '14'
};
const ubuntumap = {
    "24.04": 'noble',
    "23.10": 'manic',
    "23.04": 'lunar',
    "22.10": 'kinetic',
    "22.04": 'jammy',
    "20.04": 'focal',
    "18.04": 'bionic',
    "16.04": 'xenial',
    "14.04": 'trusty'
};


function parseVersion(string) {
    if (typeof string !== 'string' || !string) {
        throw new Error('version string is empty')
    }

    const version = new Version()

    string = string.trim()
    const colon = string.indexOf(':')
    if (colon >= 0) {
        if (colon === 0) {
            throw new Error('epoch in version is empty')
        }
        const epoch = parseInt(string.split(':')[0])
        if (isNaN(epoch)) {
            throw new Error('epoch in version is not a number')
        }
        if (epoch < 0) {
            throw new Error('epoch in version is negative')
        }
        if (epoch > Number.MAX_SAFE_INTEGER) {
            throw new Error('epoch in version is too big')
        }
        if ((colon + 1) === string.length) {
            throw new Error('nothing after colon in version number')
        }
        version.epoch = epoch
    } else {
        version.epoch = 0
    }

    const vstring = string.substring(colon + 1)
    const hyphen = vstring.lastIndexOf('-')
    if (hyphen >= 0) {
        version.version = vstring.substring(0, hyphen)
        version.revision = vstring.substring(hyphen + 1)
        if (version.revision.length === 0) {
            throw new Error('revision number is empty')
        }
    } else {
        version.version = string
        version.revision = ''
    }

    if (version.version.length === 0) {
        throw new Error('version number is empty')
    }
    if (!isDigit(version.version[0])) {
        throw new Error('version number does not start with digit' + version.version[0])
    }
    if (!version.version.match(/^[0-9a-zA-Z.+~:-]+$/)) {
        throw new Error('invalid character in version number')
    }

    if (!version.revision.match(/^[0-9a-zA-Z.+~]*$/)) {
        throw new Error('invalid character in revision number')
    }
    return version
}

function isDigit(c) {
    return typeof c === 'string' && c >= '0' && c <= '9'
}

function isAlpha(c) {
    return typeof c === 'string' && ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
}

function order(c) {
    if (isAlpha(c)) {
        return c.charCodeAt(0)
    } else if (c === '~') {
        return -1
    } else if (c) {
        return c + 256
    } else {
        return 0
    }
}

function compareVersion(a, b) {
    let pA = 0
    let pB = 0
    while (a[pA] || b[pB]) {
        let firstDiff = 0
        while ((a[pA] && !isDigit(a[pA])) || (b[pB] && !isDigit(b[pB]))) {
            const ac = order(a[pA])
            const bc = order(b[pB])

            if (ac !== bc) {
                return ac - bc
            }
            pA++
            pB++
        }
        while (a[pA] === '0') {
            pA++
        }
        while (b[pB] === '0') {
            pB++
        }
        while (isDigit(a[pA]) && isDigit(b[pB])) {
            if (!firstDiff) {
                firstDiff = a.charCodeAt(pA) - b.charCodeAt(pB)
            }
            pA++
            pB++
        }
        if (isDigit(a[pA])) {
            return 1
        }
        if (isDigit(b[pB])) {
            return -1
        }
        if (firstDiff) {
            return firstDiff
        }
    }
    return 0
}

function requestIsOs(i) {
    //console.log(i);
    return i.request.test == 'osver';
}

function requestIsSoftware(i) {
    //console.log(i.request.test);
    return i.request.test == 'software';
}

// Sort out hosts into versions
function handleVersionData(r) {
    //console.log(r);
    // First: Which machines run which version?
    if (r.result != 'OK') {
        console.log('Unexpected result from back-end server');
        return;
    }
    var osresults = r.data.filter(requestIsOs);
    var osToHost = {}
    for (var osresult of osresults) {
        osToHost[osresult.request._meta.os] = {
            distname: osresult.request._meta.distname,
            fixedpkg: osresult.request._meta.fixedpkg,
            hosts: osresult.result.map((x) => x.hostname)
        };
    }
    console.log(osToHost);
    // Now: Which machines have which package versions?
    var pkgResults = r.data.filter(requestIsSoftware);
    //console.log(pkgResults);
    var pkgToHost = {}
    for (var pkgresult of pkgResults) {
        for (var hostresult of pkgresult.result) {
            if (!Object.hasOwn(pkgToHost, hostresult.msg.pkg)) {
                pkgToHost[hostresult.msg.pkg] = {}
            }
            if (!Object.hasOwn(pkgToHost[hostresult.msg.pkg], hostresult.msg.version)) {
                pkgToHost[hostresult.msg.pkg][hostresult.msg.version] = []
            }
            pkgToHost[hostresult.msg.pkg][hostresult.msg.version].push(hostresult.hostname);
        }
    }
    for (var os in osToHost) {
        var mydiv = jQuery('#_pkgscan_' + osToHost[os].distname);
        mydiv.find(' span.hostcount').text(osToHost[os].hosts.length);
        if (osToHost[os].hosts.length > 0) {
            mydiv.find('div.inline-edit-display div').first().append(
                jQuery('<div>').addClass('form-row').addClass('_pkgscan_header').append(
                    jQuery('<div>').addClass('label').addClass('col-8').text('Package'),
                    jQuery('<div>').addClass('label').addClass('col-2').text('Unsafe'),
                    jQuery('<div>').addClass('label').addClass('col-2').text('Safe')
                )
            );

            for (var pkg in osToHost[os].fixedpkg) {
                var safevers = parseVersion(osToHost[os].fixedpkg[pkg])
                var unsafehosts = [];
                var safehosts = [];
                for (var vers in pkgToHost[pkg]) {
                    try {
                        var thisVers = parseVersion(vers);
                        //var hosts=pkgToHost[pkg][vers];
                        var thisosvershosts = pkgToHost[pkg][vers].filter(x => osToHost[os].hosts.includes(x));
                        if (thisosvershosts.length > 0) {
                            if (thisVers.compareTo(safevers) < 0) {
                                unsafehosts = unsafehosts.concat(thisosvershosts);
                            } else {
                                safehosts = safehosts.concat(thisosvershosts);
                            }
                        }
                    } catch (error) {
                        // console.log('Could not parse version '+vers);
                        // Do nothing. The version cannot be parsed.
                    }
                }
                if (unsafehosts.length + safehosts.length > 0) {
                    mydiv.find('div.inline-edit-display div').first().append(
                        jQuery('<div>').addClass('form-row').append(
                            jQuery('<div>').addClass('label').addClass('col-8').text(pkg),
                            jQuery('<div>').addClass('label').addClass('col-2').append(
                                jQuery('<a>').attr('href', 'https://hobbit.ch.cam.ac.uk/xymon-cgi/findhost.sh?host=' + (unsafehosts.map((x) => x.replace('.cam.ac.uk', '')).join('%7c'))).text(unsafehosts.length)
                            ),
                            jQuery('<div>').addClass('label').addClass('col-2').append(
                                jQuery('<a>').attr('href', 'https://hobbit.ch.cam.ac.uk/xymon-cgi/findhost.sh?host=' + (safehosts.map((x) => x.replace('.cam.ac.uk', '')).join('%7c'))).text(safehosts.length)
                            )
                        )
                    );
                } else {
                    mydiv.find('div.inline-edit-display div').first().append(
                        jQuery('<div>').addClass('form-row').append(
                            jQuery('<div>').addClass('label').addClass('col-8').text(pkg),
                            jQuery('<div>').addClass('value').addClass('col-4').text('Not installed')
                        )
                    );
                }
            }
        }
    }
}

function addPkgScanBlock(distfamily,distname,distnum) {
    return jQuery('<div>').attr('id', '_pkgscan_' + distname).addClass('box-container').addClass('col-md-6').append(
        jQuery('<div>').addClass('_pkgscan').addClass('titlebox')
        .addClass('body-content-class').addClass('card').addClass('ticket-info-cfs').append(
            jQuery('<div>').addClass('titlebox-title').addClass('card-header').append(
                jQuery('<span>').addClass('left').text(distfamily + ' ' + distname + ' [' + distnum + ']'),
            ),
            jQuery('<div>').addClass('titlebox-content').append(
                jQuery('<div>').addClass('card-body').append(
                    jQuery('<div>').addClass('inline-edit-display').append(
                        jQuery('<div>').append(
                            jQuery('<div>').addClass('form-row').append(
                                jQuery('<div>').addClass('label').addClass('col-2').text('Hosts'),
                                jQuery('<div>').addClass('value').addClass('col-10').append(
                                    jQuery('<span>').addClass('hostcount').text('??')
                                )
                            )
                        )
                    )
                )
            )
        )
    );
}

function handleUbuntuUSN(d) {
    console.log(JSON.parse(d.response))
    var resp=JSON.parse(d.response)
    var osqs=[];
    var pgqs={};
    for (var r of resp.releases) {
        jQuery('div#_pkgscan_ubuntu_' + r.codename).remove();
        console.log("Processing respose for "+r.codename)
        jQuery('div.message-stanza:contains("Ubuntu Security Notice USN")').first().append(addPkgScanBlock('Ubuntu',r.codename,r.version));
        var osq = {
            test: 'osver',
            msg: 'ubuntu\\s' + r.version,
            fields: ['hostname'],
            _meta: {
                distname: r.codename,
                os: r.version,
                fixedpkg: {}
            }
        };
        for (var p of resp.release_packages[r.codename]) {
            pgqs[p.name]={test:'software',msg:'(?<pkg>'+p.name+')\\s+(?<version>\\S+)',fields:['hostname','msg']}
            osq._meta.fixedpkg[p.name]=p.version
        }
        osqs.push(osq)

    }
    var postdata = osqs.concat(Object.values(pgqs));
    var url = '/api/rest/v1/xymondboard5.php';
    jQuery.ajax(url, {
        data: JSON.stringify(postdata),
        contentType: 'application/json',
        type: 'POST',
        success: handleVersionData,
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("Status: " + textStatus);
            alert("Error: " + errorThrown);
        }
    });
}

// Which releases are there? Look up machines running each release.
function analyseUbuntuReport() {
    const digit_start_re = new RegExp('^\\d');
    jQuery('div.message-stanza:contains("Ubuntu Security Notice USN")').each(function(i,e) {
        var msg = e.innerText;
        var matches=msg.match('Ubuntu Security Notice (USN-.*)\n');
        const usn=matches[1];
        GM_xmlhttpRequest({
            method: "GET",
            url: 'https://ubuntu.com/security/notices/'+usn+'.json',
            onload: handleUbuntuUSN
        });
    });
}

function mapSrcPkgs(r) {
    if (!Object.hasOwn(r, '_meta')) {
        return;
    }
    var pgqs = {};
    console.log('Returned data');
    console.log(r);
    const dist = r._meta.distname;
    console.log('Examining dist ' + dist);
    var last = false;
    for (var osq of window.osqs) {
        console.log(osq);
        if (!Object.hasOwn(osq, '_meta')) {
            console.log('No _meta returned from server');
            //    return;
        } else {
            if (osq._meta.distname == dist) {
                // This is the right osq for this distribution
                for (var src of Object.keys(r._meta.fixedsrc)) {
                    console.log('Source package ' + src);
                    console.log('Binaries:');
                    console.log(r[src]);
                    for (var pkg of r[src]) {
                        pgqs[pkg] = {
                            test: 'software',
                            msg: '(?<pkg>' + pkg + ')\\s+(?<version>\\S+)',
                            fields: ['hostname', 'msg']
                        };
                        osq._meta.fixedpkg[pkg] = osq._meta.fixedsrc[src];
                    }
                }
            }
        }
    }
    var postdata = window.osqs.concat(Object.values(pgqs));
    console.log('POSTDATA');
    console.log(postdata);
    var url = '/api/rest/v1/xymondboard5.php';
    jQuery.ajax(url, {
        data: JSON.stringify(postdata),
        contentType: 'application/json',
        type: 'POST',
        success: handleVersionData,
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("Status: " + textStatus);
            alert("Error: " + errorThrown);
        }
    });
}

// Get the affected source packages. Store in global variable. Make AJAX calls to convert source packages into binary packages, producing the same directory structure as the Debian case.
function analyseDebianReport() {
    var osqs = [];
    jQuery('div.message-stanza:contains("Debian Security Advisory DSA-")').each(function(i, e) {
        var msg = e.innerText;
        // Find affected source package (s?)
        var m = msg.match(new RegExp('Package\\s+: (.*)'));
        var pkg = m[1];
        // Loop over all distributions?
        while (msg.match(new RegExp('distribution \\(([^\)]*)\\).*?version (\\S+)', 's'))) {
            m = msg.match(new RegExp('distribution \\(([^\)]*)\\).*?version (\\S+)(.*)', 's'));
            var dist = m[1];
            var goodvers = m[2].replace(/\.$/, '');
            msg = m[3];
            var osq = {
                test: 'osver',
                msg: 'Debian\\s' + debmap[dist],
                fields: ['hostname'],
                _meta: {
                    distname: dist,
                    os: debmap[dist],
                    fixedsrc: {},
                    fixedpkg: {}
                }
            };
            osq._meta.fixedsrc[pkg] = goodvers;
            osqs.push(osq);
            jQuery('div#_pkgscan_' + dist).remove();
            jQuery(e).append(addPkgScanBlock('Debian',dist,debmap[dist]));
        }
    });
    // We'll store additional requests for packages in the window object for global access
    window.pgqs = {};
    window.osqs = osqs;
    console.log(window.osqs);
    // Loop over all dists, fire off some AJAX calls
    for (var osq of window.osqs) {
        var url = '/api/rest/v1/showsource/debian/' + osq._meta.distname.toLowerCase();
        jQuery.ajax(url, {
            data: JSON.stringify({
                src: Object.keys(osq._meta.fixedsrc),
                _meta: {
                    distname: osq._meta.distname,
                    fixedsrc: osq._meta.fixedsrc
                }
            }),
            contentType: 'application/json',
            type: 'POST',
            success: mapSrcPkgs,
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("Status: " + textStatus);
                alert("Error: " + errorThrown);
            }
        });
    }
}

class Version {
    compareTo(other) {
        if (this.epoch > other.epoch) {
            return 1
        }
        if (this.epoch < other.epoch) {
            return -1
        }
        const versionCompare = compareVersion(this.version, other.version)
        if (versionCompare) {
            return versionCompare
        }
        return compareVersion(this.revision, other.revision)
    }
}

(function() {
    console.log('Initialising Ubuntu / Debian security report analyser');
    'use strict';
    GM_addStyle('._pkgscan_header {font-weight: bold;}');
    analyseSecurityReport();
})();
