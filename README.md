# TampermonkeyScripts

## Getting started

Install tampermonkey [https://www.tampermonkey.net/].

Open https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/tree/release and select a script (ending .user.js). Click the 'Raw' link and Tampermonkey will intercept the download and offer to install it. 

Once it's installed, enable future updates by editing the script Settings and ticking "Check for updates". (NB: On testing, this step appears not to be required: updates are enabled by default.)

## Updating manually

In the "Installed Userscripts" tab of Tampermonkey, click to select the scripts to be updated, then "Trigger Update" from the drop-down ("--Please choose an option -- v") and click 'Start'. Newer versions will be downloaded.

