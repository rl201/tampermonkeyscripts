// ==UserScript==
// @name         CFSiblings
// @namespace    http://tampermonkey.net/
// @version      2024-04-29.0.1
// @description  A link to search for tickets with the same custom field values
// @author       Frank Lee <rl201@cam.ac.uk>
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/cfSiblings/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/cfSiblings/script.meta.js
// @match        https://tickets.ch.cam.ac.uk/rt/Ticket/Display.html?*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=cam.ac.uk
// @grant        none
// ==/UserScript==
/* globals jQuery */

(function() {
    'use strict';
    jQuery('span:contains("Custom Fields") ').closest('div.titlebox').find('div.form-row.custom-field').each(function(i,e){
        var url='/rt/Search/Simple.html?q=any+cf.'+encodeURIComponent(jQuery(e).find('div span').first().text()+jQuery(e).find('div span').last().text().trim());
        jQuery(e).find('a.othercfs').remove();
        jQuery(e).find('div').first().prepend(
            jQuery('<a>').attr('href',url).addClass('othercfs').text('[Similar tickets]')
        )
    });
})();
