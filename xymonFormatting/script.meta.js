// ==UserScript==
// @name         xymon table headers
// @namespace    http://tampermonkey.net/
// @version      0.1.3
// @description  To reformat Xymon pages
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/xymonFormatting/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/xymonFormatting/script.meta.js
// @author       You
// @require      https://cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js
// @match        https://hobbit.ch.cam.ac.uk/xymon*.html
// @match        https://hobbit.ch.cam.ac.uk/xymon-cgi/findhost.sh?*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_addStyle
// ==/UserScript==
