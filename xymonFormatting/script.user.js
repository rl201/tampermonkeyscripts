// ==UserScript==
// @name         xymon table headers
// @namespace    http://tampermonkey.net/
// @version      0.1.3
// @description  To reformat Xymon pages
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/xymonFormatting/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/xymonFormatting/script.meta.js
// @author       You
// @require      https://cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js
// @match        https://hobbit.ch.cam.ac.uk/xymon*.html
// @match        https://hobbit.ch.cam.ac.uk/xymon-cgi/findhost.sh?*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';
    
    // Your code here...
GM_addStyle('table.rotateheader tr:nth-child(1) td { height: 55px; white-space: nowrap; }');
GM_addStyle('table.rotateheader tr:nth-child(1) td > div { transform:translate(0px, 20px) rotate(315deg); width: 30px;}');
GM_addStyle('table.rotateheader tr:nth-child(1) td:nth-child(1) > div { font-weight: bolder; transform: none; width: auto;}');
GM_addStyle('table.rotateheader tr:nth-child(1) td:nth-child(1) > div span { border-bottom: none;}');
GM_addStyle('table.rotateheader tr:nth-child(1) td > div > span { border-bottom: 1px solid #ccc; padding: 5px 10px; padding-left: 0px; font-weight: normal; font-family: sans;}');
GM_addStyle('table.rotateheader tr:nth-child(1) td a { text-decoration: none;}');
GM_addStyle('table.rotateheader tr:nth-child(1) td:not(:nth-child(1)) { background-color: black;}');
GM_addStyle('table.rotateheader tr:nth-child(1) { position: sticky; top: 0;}');
GM_addStyle('table.rotateheader tr:nth-child(2) td:nth-child(1) { padding-right: 1em; }');

jQuery('table').filter(function() {
  return this.summary.match(/Group Block/);
}).addClass('rotateheader');

jQuery('table.rotateheader td').each(function(i,e){
  e=jQuery(e);
  e.removeAttr('align').removeAttr('valign').removeAttr('width').removeAttr('rowspan');
  var c=e.contents();
  e.empty().append(jQuery('<div>').append(jQuery('<span>').append(c)));
});
jQuery('table.rotateheader tr:nth-child(2) td:nth-child(1)').attr('colspan','100%');

// Attempt to remove duplicated headers
jQuery('table.rotateheader tr:not(:nth-child(1)):not(:nth-child(2)):not([class="line"])').remove()    ;

})();
