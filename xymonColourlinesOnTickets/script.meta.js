// ==UserScript==
// @name         Insert xymon colours on RT
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Load xymon colour icons when viewing tickets
// @author       Adam Thorn
// @downloadURL  https://gitlab.developers.cam.ac.uk/ch/co/tampermonkeyscripts/-/raw/main/xymonColourlinesOnTickets/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/ch/co/tampermonkeyscripts/-/raw/main/xymonColourlinesOnTickets/script.meta.js
// @match        https://tickets.ch.cam.ac.uk/rt/Ticket/Display.html?*
// @grant        none
// ==/UserScript==
