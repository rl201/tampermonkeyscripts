// ==UserScript==
// @name         Insert xymon colours on RT
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Load xymon colour icons when viewing tickets
// @author       Adam Thorn
// @downloadURL  https://gitlab.developers.cam.ac.uk/ch/co/tampermonkeyscripts/-/raw/main/xymonColourlinesOnTickets/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/ch/co/tampermonkeyscripts/-/raw/main/xymonColourlinesOnTickets/script.meta.js
// @match        https://tickets.ch.cam.ac.uk/rt/Ticket/Display.html?*
// @grant        none
// ==/UserScript==

(function() {
	'use strict';

	function replaceColors() {

		let elements = document.querySelectorAll('.messagebody .message-stanza');

		elements.forEach(element => {
			let elementContent = element.innerHTML;
			let newContent = elementContent
				.replace(/&amp;red/g, '<img src="data:image/gif;base64,R0lGODlhEAAQAPEAAIAAAP8AAAAAAAAAACH5BAEAAAIALAAAAAAQABAAAAI3lI9pEQYPVxOswVhPBo6ylGnf8XjTxglY9UVqak6vA7NkutpuGaJIKJEFVR0g6mKS0JAUhTNRAAA7">')
				.replace(/&amp;yellow/g, '<img src="data:image/gif;base64,R0lGODlhEAAQAPMAAAkFAAsGABILABwRACETACUWACwaAC8cADgiAEstAMZ3ANmCAPWTAP+ZAAAAAAAAACH5BAEAAA4ALAAAAAAQABAAAARP0EkJiDEEzE2PWotyaJw0KEzTMMpQSsWiqkvxOvHc1Hc+8y8fzRaU/YilU2rVugE8IBHpJUAkEgjBzfH8hEY3pYrleomZ5VI3CnZaDhlOBAA7">')
				.replace(/&amp;green/g, '<img src="data:image/gif;base64,R0lGODlhEAAQAPIAAAAAAD5ZJz6AJkmrNTj/AUn4BgAAAAAAACH5BAEAAAYALAAAAAAQABAAAAM4aLrcrgA8B8qQExZ7c91DiDUfGHZMaZ5jRbzvyUJCbQd43tp1jo+0m+4BEAaApN4xQ1syIcjnIwEAOw==">')
				.replace(/&amp;blue/g, '<img src="data:image/gif;base64,R0lGODlhEAAQAPIAAAAAgAEEgGkAgAAA/wBo7IAA9ZC0+wAAACH5BAEAAAcALAAAAAAQABAAAANXeLrcarAMAYAYBZpnqv8A9IBkqBTWoK4qVShDWsz0BAwKdRVET0g6RUBGgPxsASHRCAQkDzqM7xg8xHa0mQ13QF1YK9fIIiibKaYDpOQRqSM2mcZBZyQAADs=">');
			element.innerHTML = newContent;
		});
	}

	const header = document.getElementById('header');
	if (!header || !header.innerHTML.includes(' Xymon ')) {
		return;
	}

	setTimeout(replaceColors, 500);

})();
