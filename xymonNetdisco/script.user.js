// ==UserScript==
// @name         Link to netdisco from if_load pages
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Link to netdisco from if_* pages
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/xymonNetdisco/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/xymonNetdisco/script.meta.js
// @author       You
// @match        https://hobbit.ch.cam.ac.uk/xymon-cgi/svcstatus.sh?*SERVICE=if_*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @require      https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
console.log('Triggered');

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};
  var hostname=getUrlParameter('HOST');
  jQuery('div.currentmsg table tr:not(:nth-child(1)) td:nth-child(1)').each(function(i,e){
  e=jQuery(e);
  var intname=e.text();
  e.empty().append(jQuery('<a>').attr('href','https://netdisco.ch.cam.ac.uk/device?tab=ports&q='+hostname+'&prefer=port&f='+intname+'&c_admin=on&c_port=on&c_descr=on&c_comment=on&c_type=on&c_duplex=on&c_lastchange=on&c_name=on&c_speed=on&c_speed_admin=on&c_error=on&c_mac=on&c_mtu=on&c_pvid=on&c_vmember=on&c_power=on&c_ssid=on&c_nodes=on&c_neighbors=on&c_stp=on&c_up=on&mac_format=IEEE&age_num=1&age_unit=days'
                                      ).text(intname))
});
    // Your code here...
})();
