// ==UserScript==
// @name         Link to netdisco from if_load pages
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Link to netdisco from if_* pages
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/xymonNetdisco/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/xymonNetdisco/script.meta.js
// @author       You
// @match        https://hobbit.ch.cam.ac.uk/xymon-cgi/svcstatus.sh?*SERVICE=if_*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @require      https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @grant        none
// ==/UserScript==
