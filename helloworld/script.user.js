// ==UserScript==
// @name         Hello World
// @namespace    http://tampermonkey.net/
// @version      0.3.1
// @description  Introduction
// @downloadURL  https://nobody:glpat-wnXiR2pvay_vZGXfYJ-q@gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/helloworld/script.user.js
// @updateURL    https://nobody:glpat-wnXiR2pvay_vZGXfYJ-q@gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/helloworld/script.meta.js
// @author       Frank Lee <rl201@cam.ac.uk>
// @match        http://*/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    console.log('Hello world');
    // Your code here...
})();
