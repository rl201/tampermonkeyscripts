// ==UserScript==
// @name         Link access points to netdisco
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @description  Joining up bits of our intranet
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/wirelessNetdisco/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/wirelessNetdisco/script.meta.js
// @author       Frank Lee <rl201@cam.ac.uk>
// @match        https://www.wireless.cam.ac.uk/inst/*/aps/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=cam.ac.uk
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// ==/UserScript==
/* globals jQuery */

var icon='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAQAAADZc7J/AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfoAxoKABknO9NyAAAGrklEQVRIxy3Fa4yU1RkH8P/znPO+71x2Z3b2xjI7I3KRQqw0KmCLqaWKiQikMTZAoFiTRtObTfzS1hL7oU1jPzVpmmqtGECtUGKljaUUVGIMtra0igJulUWWZZeB3dmZZXdm3ts5z+mH9vflR+epgQ+pzG1M2AWqF1dc4qp8v5ADAXDA/27RBzSKlquwYEKKtIAabkgq0MvwG91U+fgy17OtdIM5GTj6ualXerNt0zT9XsBTCVO/H8bh5BM0ryi2uJyZNg+lv9TXtU5oH0/pUW4YxTkOXWozugcvd1/fR7c54xw7kABgD+9nd+1qhdS2QEElaKclvQJDlgd52EwnyFo1ExlQLk7nknsj/xU7BVKeVOwwe8x2ig5sShu2HTuf/Bftaej8nGxPBkEvqzE+yykcQcAQMJwC6M0+yXWGkt+D/G2ZSRXe24AYhgO+ZrduJnz62ve95XaZ4x5pqkCbBAYZCKVgeCaF2Tj37MVonCyb9NLshfvnxFECkP/jZHvBfcd8e7hwp028pnBblU0n0Rl4aINdFgYRB87rhOdowHM1Wyv5T/idiGCyWv6TrP1KejBdL19Whx7ffMKsYB2gWwyRgiVxgHLJKhmjT9Q74bul+a/qx4zMP3B470h9tXcL59NpIiIGACaRmmSh7nPng9hLQwBZWBfmgreKxzdddY/Pvvmt5JHSnu7k+i8a6dz7xz576gtzl+9yu8+99mdZQxft1n1nq15sGaSdc2AQhNDLI/7IN9wDhcmXBuU+NGBdSk236cBgzyQ9+OkjH3t/5Pmm92v9tGu+rq3zQUd0Q16FysOciVb5ef/f8UeDFb3u2gEplIt+YeyqzdyYUn18p7xTvWoxuc3v0J+S5WqF78KnzAWlttC4amhrgQUMPtbb7A/csumQmgPKj5NKVOSpvGU/yEWZ9ly3673gl1ur0E9ifKohBzpKJzJ1NdtWKp893Df3vFrJI7JVd8shOyxjA7t8PbkPN3i1/PbZWXWIVpoR/+s7ZlumErWCfn1DyPNqKA1jlYG3fz7cwOtloZE7Yl5hbkGZqzGaCVd42NwcLb8zcc4u5C/Rxr1tuMlsG8VogDmLkiQCrdCl5W5iBze50cgil3FgGImFHOCyZvgui5oDKbl7gfIE2soSyUPP2NGgm9udIpcHRosyoVhdejJSoiadMxMlLqhrkyDFpJ7qeKNUEyuFngG/kbS7vQ8ySxLax6PeuOrESk0Hp/vmUfCK85unD/agN3Ht6I5ZRad6yMvrTPPhmf0DYbFlKL1jtt9InPcrWJpyP9+UtiLknHqrAzeYbacTcT2T11OxlaHsXHo2Kvl5PR1Fcj4/b+tRwSt6p0Iy6OrYTXGZ6KC6xKcp5RyO5hv73a1w9Jz5qf+o+REBteI2kdYhN+ycfip5Vj8pjxDow/5dG1st8rHCfAY8KHWV8WxszG3iOlLGsFpCYiNVdVUZYj/XZctSUVVEnuBGHpZhO7dKQloY5VwnmBGe5gUmTFSQ6L/O6uMsDqby+W6/ZkMCk7XzIYNhQ1Vb3eWqAhb/xPFZi1qmje64jzlP/ZJYaBC5/HH1NmqCa6X8uDuDK2oiicOEJ+iKO6NH672O3RV+u+91crDwrbvdFkD76Lx/mTuJx+J3zLGe7pI2XeODPFaN3Hxn3ZzifxTgFdSKKyOpqVImbHxxpsuHdUm3X8bSlAEAyGFU1VUOv5269TK4VlpsF9XhiAiAALKoXrYzJdDtl3dP5R3+jwjQ/QrpR1gZHNoUxS8cPdI1aqa26HV/++4lR8+p/L8ejIT+gPY/d1zw6Jnm30/+qpQhQgea8p1orfEUt12df4j9xfSb9NgP+tZE99jhPXy4XpEpdcT2qyATSB/9RabCKr26ZM8uawQWGoxU8RluQb3ierO7t8z8DOvV4tbtx+o9EzdL572u+ru5xofX28VPuvT1mdzec56uj7+3Ae0AESzykIURae17qX5DPW2ShDVgwQiC1DrzOZ+QROtp/HkNK0v3VCSWxWqRLw4RfAAhVC3wkYt7mY7oKXnY3VS++KJNhx7aOX8tDh3lYRHC4wx1MjbMiUMIDxmEMMgB1HGashR+z7SUugc17wXvjYZuZE4/8/5Jo33PMzFLNnAujT0fXpKQZAIHE/na900CyQSgNCzqOcWifoePWdNP7MjY6rHAbrZNlcVaU4LVw7LTTnNAt5ohWL3QPmpryqdVpoJUDcoOm3AXrbTqYeqRhnh6jRTjAfKVsllTVl0Ik0VEyrOBqaguJEkVwp54psrdZNIyPEWy1AzhvwcAk4q2YM+zAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDI0LTAzLTI2VDA5OjU4OjM4KzAwOjAwLLMlogAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMC0xMS0yNFQxMzoyNTo1OCswMDowMCrGBi4AAAAASUVORK5CYII=';

(function() {
    'use strict';
    console.log(jQuery('table#aps tbody td:nth-child('+(1+jQuery('th:contains("MAC Addr")').prevAll().length)+')'));
    jQuery('table#aps tbody td:nth-child('+(1+jQuery('th:contains("MAC Addr")').prevAll().length)+')').each(function(i,e){
      var mac=jQuery(e).text();
      jQuery(e).append(
        jQuery('<a>').attr('href','https://netdisco.ch.cam.ac.uk/search?tab=node&stamps=on&deviceports=on&daterange=&mac_format=IEEE&q='+mac).append(
          jQuery('<img>').attr('src',icon)
        )
      );
    });
})();



