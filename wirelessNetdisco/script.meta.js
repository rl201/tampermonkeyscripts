// ==UserScript==
// @name         Link access points to netdisco
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @description  Joining up bits of our intranet
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/wirelessNetdisco/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/wirelessNetdisco/script.meta.js
// @author       Frank Lee <rl201@cam.ac.uk>
// @match        https://www.wireless.cam.ac.uk/inst/*/aps/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=cam.ac.uk
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// ==/UserScript==
