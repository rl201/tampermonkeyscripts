// ==UserScript==
// @name         Drag and Drop with AJAX
// @namespace    http://tampermonkey.net/
// @version      1.1.2
// @description  Add drag-and-drop functionality to a webpage and trigger AJAX on drop
// @author       Frank Lee <rl201@cam.ac.uk>
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/DragTicketsToMerge/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/DragTicketsToMerge/script.meta.js
// @match        https://tickets.ch.cam.ac.uk/rt/
// @match        https://tickets.ch.cam.ac.uk/rt/Search/Results.html*
// @grant        none
// ==/UserScript==
