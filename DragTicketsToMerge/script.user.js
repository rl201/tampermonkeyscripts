// ==UserScript==
// @name         Drag and Drop with AJAX
// @namespace    http://tampermonkey.net/
// @version      1.1.2
// @description  Add drag-and-drop functionality to a webpage and trigger AJAX on drop
// @author       Frank Lee <rl201@cam.ac.uk>
// @downloadURL  https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/DragTicketsToMerge/script.user.js
// @updateURL    https://gitlab.developers.cam.ac.uk/rl201/tampermonkeyscripts/-/raw/release/DragTicketsToMerge/script.meta.js
// @match        https://tickets.ch.cam.ac.uk/rt/
// @match        https://tickets.ch.cam.ac.uk/rt/Search/Results.html*
// @grant        none
// ==/UserScript==
/* globals jQuery */

(function() {
    'use strict';
    let confirmationEnabled = true;
    function toggleConfirmation(enabled) {
        confirmationEnabled = enabled;
    }

    function showConfirmation(msg) {
        return new Promise((resolve) => {
            if (!confirmationEnabled) {
                resolve(true); // Skip confirmation if disabled
                return;
            }

            // Create confirmation dialog
            const confirmationDialog = document.createElement('div');
            confirmationDialog.style.position = 'fixed';
            confirmationDialog.style.top = '50%';
            confirmationDialog.style.left = '50%';
            confirmationDialog.style.transform = 'translate(-50%, -50%)';
            confirmationDialog.style.padding = '20px';
            confirmationDialog.style.backgroundColor = 'white';
            confirmationDialog.style.border = '1px solid #ccc';
            confirmationDialog.style.boxShadow = '0px 0px 10px rgba(0, 0, 0, 0.1)';
            confirmationDialog.style.zIndex = '1000';
            confirmationDialog.innerHTML = `
                <h3>${msg}</h3>
                <label>
                    <input type="checkbox" id="disableFutureConfirmation"> Don't ask again
                </label>
                <br>
                <button id="confirmBtn">Confirm</button>
                <button id="cancelBtn">Cancel</button>
            `;
            document.body.appendChild(confirmationDialog);

            // Event listeners for confirmation
            document.getElementById('confirmBtn').addEventListener('click', () => {
                if (document.getElementById('disableFutureConfirmation').checked) {
                    toggleConfirmation(false);
                }
                document.body.removeChild(confirmationDialog);
                resolve(true); // Proceed with action
            });

            document.getElementById('cancelBtn').addEventListener('click', () => {
                document.body.removeChild(confirmationDialog);
                resolve(false); // Cancel action
            });
        });
    }

    // Function to enable drag-and-drop on specific elements
    function makeDraggableAndDroppable() {
        document.querySelectorAll('table.ticket-list tbody').forEach(el => {
            if (!el.hasAttribute('draggable')) {
                el.setAttribute('draggable', 'true');
                el.addEventListener('dragstart', (event) => {
                    const recordId = el.getAttribute('data-record-id');
                    if (recordId) {
                        event.dataTransfer.setData('text/plain', recordId);
                    }
                });
            }
        });

        // Handle tickets landing on tickets
        document.querySelectorAll('table.ticket-list tbody').forEach(el => {
            if (!el.dataset.droppableInit) {
                el.dataset.droppableInit = true; // Prevent duplicate listeners

                el.addEventListener('dragover', (event) => {
                    event.preventDefault();
                    el.classList.add('drag-over');
                });

                el.addEventListener('dragleave', () => {
                    el.classList.remove('drag-over');
                });

                // ✅ Convert event listener into an async function
                el.addEventListener('drop', async function(event) {
                    event.preventDefault();
                    el.classList.remove('drag-over');

                    let draggedRecordId = event.dataTransfer.getData('text/plain');
                    let draggedElement = document.querySelector(`[data-record-id="${draggedRecordId}"]`);

                    if (draggedElement) {
                        var msg=`Are you sure you want to merge ticket ${draggedElement.getAttribute('data-record-id')} into ticket ${el.getAttribute('data-record-id')}?`
                        const confirmed = await showConfirmation(msg); // ✅ Await confirmation
                        if (confirmed) {
                            sendAjaxRequest(draggedRecordId, el);
                        }
                    }
                });
            }

        });

        // Handle tickets landing on queues
        document.querySelectorAll('table.queue-summary tr').forEach(el => {
            if (!el.dataset.droppableInit) {
                el.dataset.droppableInit = true; // Prevent duplicate listeners

                el.addEventListener('dragover', (event) => {
                    event.preventDefault();
                    el.classList.add('drag-over');
                });

                el.addEventListener('dragleave', () => {
                    el.classList.remove('drag-over');
                });

                // ✅ Convert event listener into an async function
                el.addEventListener('drop', async function(event) {
                    event.preventDefault();
                    el.classList.remove('drag-over');

                    let draggedRecordId = event.dataTransfer.getData('text/plain');
                    let draggedElement = document.querySelector(`[data-record-id="${draggedRecordId}"]`);

                    if (draggedElement) {
                        var queue=jQuery(el).find('td').first().text().trim();
                        var ticket=draggedElement.getAttribute('data-record-id')
                        var msg=`Are you sure you want to move ticket ${ticket} into queue ${queue}?`
                        const confirmed = await showConfirmation(msg); // ✅ Await confirmation
                        if (confirmed) {
                            let data= new URLSearchParams();
                            data.append("content",`Queue: ${queue}`);
                            editTicket(ticket, data, 'updated');
                        }
                    }
                });
            }
        });

        // Handle tickets landing on icons
        document.querySelectorAll('li[droppable][action="setstatus"]').forEach(el => {
            if (!el.dataset.droppableInit) {
                el.dataset.droppableInit = true; // Prevent duplicate listeners

                el.addEventListener('dragover', (event) => {
                    event.preventDefault();
                    el.classList.add('drag-over');
                });

                el.addEventListener('dragleave', () => {
                    el.classList.remove('drag-over');
                });

                // ✅ Convert event listener into an async function
                el.addEventListener('drop', async function(event) {
                    event.preventDefault();
                    el.classList.remove('drag-over');

                    let draggedRecordId = event.dataTransfer.getData('text/plain');
                    let draggedElement = document.querySelector(`[data-record-id="${draggedRecordId}"]`);

                    if (draggedElement) {
                        var status=jQuery(el).attr('status').trim();
                        var ticket=draggedElement.getAttribute('data-record-id')
                        var msg=`Are you sure you want to set ticket ${ticket}'s status to ${status}?`
                        const confirmed = await showConfirmation(msg); // ✅ Await confirmation
                        if (confirmed) {
                            let data= new URLSearchParams();
                            data.append("content",`Status: ${status}`);
                            editTicket(ticket, data, 'updated');
                        }
                    }
                });
            }
        });
    }

    function editTicket(ticket, data, match) {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', `/rt/REST/1.0/ticket/${ticket}/edit`, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
                if (xhr.responseText.match(match)) {
                    document.querySelector(`[data-record-id="${ticket}"]`).remove();
                }
            }
        };
        xhr.send(data.toString());
    }
    // Example AJAX function to send dropped data
    function sendAjaxRequest(draggedRecordId, dropTarget) {
        let xhr = new XMLHttpRequest();
        var droppedOnRecordId= dropTarget.getAttribute('data-record-id') || null
        if (droppedOnRecordId != null) {
            xhr.open('POST', '/rt/REST/1.0/ticket/'+draggedRecordId+'/merge/'+droppedOnRecordId, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    // console.log('Server Response:', xhr.responseText);
                    if (xhr.responseText.match('Merge completed')) {
                        document.querySelector(`[data-record-id="${draggedRecordId}"]`).remove();
                    }
                }
            };
            xhr.send();
        }
    }
    var trashcan=jQuery('<svg height="32px" version="1.1" viewBox="0 0 32 32" width="32px" xmlns="http://www.w3.org/2000/svg" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" xmlns:xlink="http://www.w3.org/1999/xlink"><g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1"><g fill="#157EFB" id="icon-27-trash-can"><path d="M23,7 L21,7 L21,7 L21,5 C21,3.9 20,3 10,3 L14,3 C13,3 12,4 12,5 L12,7 L10,7 L6,7 L6,8 L8,8 L8,27 C8,28.6537881 9.3,30 11,30 L22,30 C23.6567977,30 25,28.6640085 25,27 L25,8 L27,8 L27,7 L23,7 L23,7 L23,7 Z M9,8 L9,27 C9,28 10,29 11,29 L22,29 C23,29 24,28 24,27 L24,8 L9,8 L9,8 Z M12,10 L12,27 L13,27 L13,10 L12,10 L12,10 Z M16,10 L16,27 L17,27 L17,10 L16,10 L16,10 Z M20,10 L20,27 L21,27 L21,10 L20,10 L20,10 Z M14,4 C13.4490268,4 13,4.44266033 13,5 L13,7 L20,7 L20,5 C20,4.44724809 19.6,4 19,4 L14,4 L14,4 Z" id="trash-can"/></g></g></svg>');
    var thumbsup=jQuery('<svg height="32px" version="1.1" viewBox="0 0 32 32" width="32px" xmlns="http://www.w3.org/2000/svg" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" xmlns:xlink="http://www.w3.org/1999/xlink"><title/><desc/><defs/><g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1"><g fill="#157EFB" id="icon-6-thumb-up"><path d="M20.2999878,14 L27.5069036,14 C28.3361142,14 29,14.6715729 29,15.5 C29,16.3342028 28.331518,17 27.5069036,17 L24,17 L24,18 L26.5069036,18 C27.3361142,18 28,18.6715729 28,19.5 C28,20.3342028 27.331518,21 26.5069036,21 L23,21 L23,21 L23,22 L25.5069036,22 C26.3361142,22 27,22.6715729 27,23.5 C27,24.3342028 26.331518,25 25.5069036,25 L22,25 L22,26 L23.4983244,26 C24.3288106,26 25,26.6715729 25,27.5 C25,28.3342028 24.3276769,29 23.4983244,29 L19.7508378,29 L19.7508378,29 L16,29 L10,27 L4.00292933,27 C3.43788135,27 3,26.5529553 3,26.0014977 L3,14.9985023 C3,14.4474894 3.44769743,14 3.9999602,14 L10.5,14 C10.5000062,13.9999986 13.804736,13.2763148 15.3710938,10.3012695 C16.9374527,7.32622128 15.1291504,3.00000043 18,3 C21.1514893,2.99999957 21.5007324,8.5 20.2999878,14 L20.2999878,14 Z" id="thumb-up"/></g></g></svg>');
    // Let's add some UI to represent "Rejected", "Resolved"
    jQuery('div#main-navigation > ul').append(
        jQuery('<li>').attr('title','Drop a ticket here to reject it.').attr('droppable','').attr('action','setstatus').attr('status','rejected').append(trashcan),
        jQuery('<li>').attr('title','Drop a ticket here to resolve it.').attr('droppable','').attr('action','setstatus').attr('status','resolved').append(thumbsup)
    );

    // Run the function when the page loads
    makeDraggableAndDroppable();

})();
